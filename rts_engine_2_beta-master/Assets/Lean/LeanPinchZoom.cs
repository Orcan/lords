
using UnityEngine;
using Lean.Touch;
using Lean.Common;
using FSA = UnityEngine.Serialization.FormerlySerializedAsAttribute;
using System;

public class LeanPinchZoom : MonoBehaviour // никак не участвует  в игре 
{
    public LeanFingerFilter Use = new LeanFingerFilter(true);
    public Camera cameraZoom;
    public float MaxZoom  = 100; // size 62.89
    public float MinZoom = 10;  // 5
    public float ForceZoom = 3;
    public enum CameraMunipEntyte { fieldOfView, Size, Scale };
    public CameraMunipEntyte cameraMunipEntyte;
    private void Start()
    {
        var fingers = Use.UpdateAndGetFingers();
           var pinchScale = LeanGesture.GetPinchScale(fingers); 

    }
    void Update()
    {
        //     var oldScale = transform.localPosition;

    if(CameraMunipEntyte.fieldOfView == cameraMunipEntyte)    MunFieldOfView();
    if(CameraMunipEntyte.Size == cameraMunipEntyte) MunSize();

    }

    private void MunSize()
    {
        var fingers = Use.UpdateAndGetFingers();

        // Calculate pinch scale, and make sure it's valid Рассчитайте шкала сжимания и убедитесь, что это действительно 
        var pinchScale = LeanGesture.GetPinchScale(fingers);
        //    Debug.Log(oldScale);
        //     Debug.Log(fingers);
        Debug.Log(pinchScale);


        if (pinchScale != 1)
        {

            if (pinchScale < 1 && cameraZoom.orthographicSize < MaxZoom)
            {
                cameraZoom.orthographicSize -= (pinchScale - 1) * ForceZoom;
            }
            else if (pinchScale > 1 && cameraZoom.orthographicSize > MinZoom)
            {
                cameraZoom.orthographicSize -= (pinchScale - 1) * ForceZoom;
            }

        }
    }

    void MunFieldOfView()
    {    // Get the fingers we want to use/ получить пальцы, которые мы хотим использовать 
        var fingers = Use.UpdateAndGetFingers();

        // Calculate pinch scale, and make sure it's valid Рассчитайте шкала сжимания и убедитесь, что это действительно 
        var pinchScale = LeanGesture.GetPinchScale(fingers);
        //    Debug.Log(oldScale);
        //     Debug.Log(fingers);
        Debug.Log(pinchScale);


        if (pinchScale != 1)
        {

            if (pinchScale < 1 && cameraZoom.fieldOfView < MaxZoom)
            {
                cameraZoom.fieldOfView -= (pinchScale - 1) * ForceZoom;
            }
            else if (pinchScale > 1 && cameraZoom.fieldOfView > MinZoom)
            {
                cameraZoom.fieldOfView -= (pinchScale - 1) * ForceZoom;
            }
      
        }
      
    }

}
 