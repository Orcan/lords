using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    public string sceneToLoad;
    public TextMeshProUGUI TextMesh;
    public string mainScene = "MainTouch";
    private void Start()
    {
        sceneToLoad = GetComponentInChildren<TextMeshProUGUI>().text;
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void MainScene()
    {
        SceneManager.LoadScene(mainScene);
    }



    private void Update()
    {
       if(TextMesh == true) TextMesh.text = SceneManager.GetActiveScene().name; 
    }




}
