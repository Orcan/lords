﻿using System;
using UnityEngine;

namespace RTSEngine.Determinism
{
    public struct IntValues
    {
        public int Item1;
        public int Item2;
    }

    public struct CommandInput
    {
        public byte sourceMode; //input source's mode // Режим входного источника
        public byte targetMode; //input's target mode // Целевой режим ввода

        public string code; //a string that holds a group of unit sources for group unit related commands or just a task code for other commands
                            //or the code of the entity component attached to the source object which will launch the command.
                            // Строка, которая содержит группу источников единиц для групповых команд, связанных с группой или просто код задач для других команд
                            // или код компонента объекта, прикрепленный к исходному объекту, который запустит команду.



        public bool isSourcePrefab;
        public int sourceID; //object that launched the command // Объект, который запустил команду
        public Vector3 sourcePosition; //initial position of the source obj // начальная позиция источника OBJ 

        // TargetData
        public int targetID; //target object that will get the command // целевой объект, который получит команду

        public Vector3 targetPosition;  // целевая позиция.
        // Это поле позволяет добавлять 3 дополнительных значения поплавка в структуре ввода сети.
        public Vector3 opPosition; //this field allows to add 3 extra float values in the network input struct.
        // Эта команда ввода была запрошена непосредственно игроком? +
        public bool playerCommand; //has this input command been requested directly by the player?+
        // дополнительный атрибут int 
        public IntValues intValues; //extra int attribute 
        public float floatValue; //extra float attribute

        // DEV:
        public string sourceCode;
    }
}