﻿using System;
using RTSEngine.Attack;

// Debug the DropOffSource state never going to goingBack
// Add events for when the DropOffSource state is updated.
// Fix formation issue

namespace RTSEngine.Event
{
    public class AttackLaunchEventArgs : EventArgs
    {
        public AttackObjectLaunchLog LaunchLog { private set; get; }

        public AttackLaunchEventArgs(AttackObjectLaunchLog launchLog)
        {
            LaunchLog = launchLog;
        }
    }
}
