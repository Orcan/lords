﻿using UnityEngine;
using UnityEngine.AI;

using RTSEngine.Entities;
using RTSEngine.EntityComponent;
using RTSEngine.Game;
using RTSEngine.Logging;
using System.Collections;

namespace RTSEngine.Movement
{
    public class NavMeshAgentController : MonoBehaviour, IMovementController
    {
        #region Attributes
        public bool Enabled
        {
            set { navAgent.enabled = value; }
            get => navAgent.enabled;
        }

        public bool IsActive
        {
            set
            {
                if (navAgent.isOnNavMesh)
                    navAgent.isStopped = !value;
            }
            get => !navAgent.isStopped;
        }

        private NavMeshAgent navAgent; 
        private NavMeshPath navPath;

        private MovementControllerData data;
        public MovementControllerData Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;

                navAgent.speed = data.speed;
                navAgent.acceleration = data.acceleration;

                navAgent.angularSpeed = data.angularSpeed;

                navAgent.stoppingDistance = data.stoppingDistance;
            }
        }

        /// <summary>
        /// /// Навигационная мешская область маска, в которой устройство может перемещаться.
        /// The navigation mesh area mask in which the unit can move.
        /// </summary>
        public LayerMask NavigationAreaMask => navAgent.areaMask;

        /// <summary>
        /// /// Размер, который блока занимает в навигационной сетке во время движения.
        /// The size that the unit occupies in the navigation mesh while moving.
        /// </summary>
        public float Radius => navAgent.radius;

        /// <summary>
        /// /// Положение следующего угла активного пути устройства.
        /// The position of the next corner of the unit's active path.
        /// </summary>
        public Vector3 NextPathTarget { get { return navAgent.steeringTarget; } }

        /// <summary>
        /// /// Положение последнего угла активного пути устройства, ака, пункт назначения.
        /// The position of the last corner tof the unit's active path, AKA, the path's destination.
        /// </summary>
        public Vector3 FinalTarget { get { return navAgent.destination; } }

        public bool IsLastPlayerCommand { private set; get; }

        // Game services
        protected IGameLoggingService logger { private set; get; }

        // Other components
        protected IGameManager gameMgr { private set; get; }
        protected IMovementComponent mvtComponent { private set; get; }
        #endregion

        #region Initializing/Terminating
        public void Init(IGameManager gameMgr, IMovementComponent mvtComponent, MovementControllerData data)
        {
            this.gameMgr = gameMgr;
            this.mvtComponent = mvtComponent;

            this.logger = gameMgr.GetService<IGameLoggingService>(); 

            IEntity entity = mvtComponent?.Entity;
            if (!logger.RequireValid(entity
                , $"[{GetType()}] Can not initialize without a valid Unit instance."))
                return;

            navAgent = entity.gameObject.GetComponent<NavMeshAgent>();
            if (!logger.RequireValid(navAgent,
                $"[NavMeshAgentController - '{entity.Code}'] NavMeshAgent component must be attached to the unit."))
                return;
            navAgent.enabled = true;

            this.Data = data;

            navPath = new NavMeshPath();
            // Всегда устанавливаем никому, как многопользовательская игра из препятствий Navmesh, поскольку она далеко не детерминированная
            // Always set to none as Navmesh's obstacle avoidance desyncs multiplayer game since it is far from deterministic
            navAgent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
            // Убедитесь, что компонент Navmeshagent обновляет позицию нашего устройства при активном режиме.
            // Make sure the NavMeshAgent component updates our unit's position when active.
            navAgent.updatePosition = true;
        }
        #endregion

        #region Preparing/Launching Movement
        /// <summary>
        /// /// Попытки рассчитать действительный путь для указанной позиции назначения.
        /// Attempts to calculate a valid path for the specified destination position.
        /// </summary>
        /// <param name="destination">Вектор3, который представляет целевую позицию движения Vector3 that represents the movement's target position.</param>
        /// <returns>ИСТИНА, если путь действителен и завершит, в противном случае ложь. True if the path is valid and complete, otherwise false.</returns>
        public void Prepare(Vector3 destination, bool playerCommand)
        {
            this.IsLastPlayerCommand = playerCommand;
            // вот тут если это командир, то нужно рисовать от Объекта Этого Путь до destination
            DrawArrowMove(destination); // ORCAN


            navAgent.CalculatePath(destination, navPath);

            if (navPath != null && navPath.status == NavMeshPathStatus.PathComplete)
                mvtComponent.OnPathPrepared();
            else
                mvtComponent.OnPathFailure();
        }
        /////////////////////////     Orcan      /////////////////////////
        private LineRenderer rope = null;
        public void DrawArrowMove(Vector3 _destination)
        {

            if (this.gameObject.GetComponent<LineRenderer>() != null)
            {
                rope = this.gameObject.GetComponent<LineRenderer>();
                rope.SetPosition(0, gameObject.transform.position);
                rope.SetPosition(1, _destination);
              
                StartCoroutine(DrawArrowMoveCorountine(_destination));
            }
          
          
        }

        IEnumerator DrawArrowMoveCorountine(Vector3 _destination)
        {
           // rope.positionCount = 2;
            while (Vector3.Distance(transform.position, _destination) >1f)
            {
              rope.SetPosition(0, gameObject.transform.position);

            
            //    Debug.Log(Vector3.Distance(transform.position, _destination));
                yield return null;
            }
            rope.SetPosition(0, gameObject.transform.position);
            rope.SetPosition(1, gameObject.transform.position);
            // rope.positionCount = 0;
        }
        /////////////////////////        /////////////////////////



        /// <summary>
        /// /// Запускает движение устройства с использованием последнего расчетного пути от метода «Preepare ()».
        /// Starts the unit movement using the last calculated path from the "Prepare()" method.
        /// </summary>
        public void Launch ()
        {
            IsActive = true;

            navAgent.SetPath(navPath);
        }
        #endregion
    }
}
