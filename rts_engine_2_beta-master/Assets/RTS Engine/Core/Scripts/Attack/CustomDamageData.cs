﻿using RTSEngine.Entities;
using UnityEngine;

namespace RTSEngine.Attack
{
    [System.Serializable]
    public struct CustomDamageData
    {
        [Tooltip("[Подбор подсказки («Определите коды или категории объектов, которые будут иметь дело пользовательского значения ущерба».)]Define the codes or categories of entities that will be dealt a custom damage value.")]
        public CodeCategoryField code;
        [Tooltip("Введите пользовательское значение ущерба для сделки  Input the custom damage value to deal.")]
        public int damage;
    }
}
