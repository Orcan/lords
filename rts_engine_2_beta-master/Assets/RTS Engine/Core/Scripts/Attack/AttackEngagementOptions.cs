﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTSEngine.Attack
{
    [System.Serializable]
    public struct AttackEngagementOptions
    {
        [Tooltip("Включите цели, которые назначают локальный игрок?   Engage targets that the local player assigns?")]
        public bool engageOnAssign; 
        [Tooltip("Когда ущерб относится к сущности фракции, он попытается участвовать в источнике ущерба. When damage is dealt to the faction entity, it will attempt to engage with the source of the damage.")]
        public bool engageWhenAttacked;
        [Tooltip("Запустите одну итерацию атаки, а затем остановить атаку? Trigger one attack iteration and then stop the attack?")]
        public bool engageOnce;
        [Tooltip("Заниматься дружественным фракционным организациями? Engage friendly faction entities?")]
        public bool engageFriendly;
        [Tooltip("Автоматические объекты, которые заблокированы углом LOS (злоумышленник не смотрит на цель)? Это относится только к автоматическим объектам. Auto-engage entities that are blocked by the LOS angle (attacker not looking at target)? This only applies for auto-targeting entities.")]
        public bool autoIgnoreAngleLOS;
        [Tooltip("Автоматические объекты, которые блокируются препятствиями? Это относится только к автоматическим объектам. Auto-engage entities that are blocked by obstacles? This only applies for auto-targeting entities.")]
        public bool autoIgnoreObstacleLOS;
    }
}
