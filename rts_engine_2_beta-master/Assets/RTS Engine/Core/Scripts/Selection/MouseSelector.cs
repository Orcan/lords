﻿using UnityEngine;
using UnityEngine.EventSystems;

using RTSEngine.Entities;
using RTSEngine.Game;
using RTSEngine.BuildingExtension;
using RTSEngine.Cameras;
using RTSEngine.Terrain;
using RTSEngine.Task;
using RTSEngine.EntityComponent;
using RTSEngine.UI;
using RTSEngine.Controls;
using RTSEngine.Logging;

namespace RTSEngine.Selection
{
    public class MouseSelector : MonoBehaviour, IMouseSelector
    {
        #region Attributes
        [SerializeField, Tooltip("Включите, чтобы игрок дважды щелкнуть объект, чтобы выбрать все объекты того же типа в определенном диапазоне.")]
        private bool enableDoubleClickSelect = true;
        [SerializeField, Tooltip("Сущности того же типа в пределах этого диапазона исходного двойного щелчка будут выбраны."), Min(0.0f)]
        private float doubleClickSelectRange = 10.0f;

        [Space(), SerializeField, Tooltip("Определите ключ, используемый для выбора нескольких объектов, когда удерживается.")]
        private ControlType multipleSelectionKey = null;
        public bool MultipleSelectionKeyDown => controls.Get(multipleSelectionKey);

        [Header("Layers"), SerializeField, Tooltip("Введите имя слоя, которое будет использоваться для объектов выбора объектов.")]
        private string entitySelectionLayer = "EntitySelection";
        public string EntitySelectionLayer => entitySelectionLayer;

        [SerializeField, Tooltip("Введите области местности, которые клики для игрока.")]
        private TerrainAreaType[] clickableTerrainAreas = new TerrainAreaType[0];
        // Это будет включать слои, определенные в слое выбора ClickabletermainaRarareas и Entity
        // This would incldue the layers defined in the clickableTerrainAreas and entitySelectionLayer
        private LayerMask clickableLayerMask;

        [Header("Selection Flash")]
        [SerializeField, Tooltip("Продолжительность отбора маркера вспышки.")]
        private float flashTime = 1.0f;
        [SerializeField, Tooltip("Как часто вспышка маркера выбора?")]
        private float flashRepeatTime = 0.2f;

        [SerializeField, Tooltip("Цвет, используемый, когда мигает маркер выбора дружелюбного объекта.")]
        private Color friendlyFlashColor = Color.green;
        [SerializeField, Tooltip("Цвет используется, когда маркер отбора вражеской сущности мигает.")]
        private Color enemyFlashColor = Color.red; 

        // Game services
        protected IGameManager gameMgr { private set; get; }
        protected IGameUIManager gameUIMgr { private set; get; } 
        protected ISelectionManager selectionMgr { private set; get; } 
        protected IBuildingPlacement placementMgr { private set; get; }
        protected ITerrainManager terrainMgr { private set; get; }
        protected ITaskManager taskMgr { private set; get; }
        protected IMainCameraController mainCameraController { private set; get; }
        protected IGameControlsManager controls { private set; get; }
        protected IGameLoggingService logger { private set; get; } 
        #endregion

        #region Initializing/Terminating
        public void Init(IGameManager gameMgr)
        {
            this.gameMgr = gameMgr;

            this.gameUIMgr = gameMgr.GetService<IGameUIManager>(); 
            this.placementMgr = gameMgr.GetService<IBuildingPlacement>();
            this.selectionMgr = gameMgr.GetService<ISelectionManager>();
            this.terrainMgr = gameMgr.GetService<ITerrainManager>();
            this.taskMgr = gameMgr.GetService<ITaskManager>();
            this.mainCameraController = gameMgr.GetService<IMainCameraController>();
            this.controls = gameMgr.GetService<IGameControlsManager>();
            this.logger = gameMgr.GetService<IGameLoggingService>();
           
            if (!logger.RequireValid(multipleSelectionKey,
              // Поле «Многократный ключ выбора» не был назначен! Функциональность будет отключена.
              $"[{GetType().Name}] Field 'Multiple Selection Key' has not been assigned! Functionality will be disabled.",
              type: LoggingType.warning))
                return; 

            clickableLayerMask = new LayerMask();

            clickableLayerMask |= (1 << LayerMask.NameToLayer(entitySelectionLayer));
            //Поле кликабельной местности областей имеет некоторые неверные элементы!
            if (!logger.RequireValid(clickableTerrainAreas,
              $"[{GetType().Name}] 'Clickable Terrain Areas' field has some invalid elements!"))
                return; 

            foreach(TerrainAreaType area in clickableTerrainAreas)
                clickableLayerMask |= area.Layers;
        }
        #endregion

        #region Handling Mouse Selection
        //bool leftButtonDown = Input.GetMouseButtonDown(0);
        //bool rightButtonDown = Input.GetMouseButtonDown(1);
        public   bool reverseMause = false;
        private void Update()
        {
            if (gameMgr.State != GameStateType.running 
                || placementMgr.IsPlacingBuilding
                || !gameUIMgr.HasPriority(this)
                || EventSystem.current.IsPointerOverGameObject())
                return;
            bool leftButtonDown = Input.GetMouseButtonDown(0);
            bool rightButtonDown = Input.GetMouseButtonDown(1);
        
            // Если указатель мыши находится над элементом интерфейса UI или MiniMap, мы не будем обнаруживать выбор сущности
            // Кроме того, мы уверены, что одна из кнопок мыши снижается
            // If the mouse pointer is over a UI element or the minimap, we will not detect entity selection
            // In addition, we make sure that one of the mouse buttons are down
            if (!leftButtonDown && !rightButtonDown)
                return;

            if (Physics.Raycast(mainCameraController.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, Mathf.Infinity, clickableLayerMask.value))
            {// Получить объект, который игрок нажал или посмотреть, щелкнул ли игрок на зоне местности.
                // Get the entity that the player clicked in or see if the player clicked on a terrain area.
                IEntity hitEntity = hit.transform.gameObject.GetComponent<EntitySelectionCollider>()?.Entity; 
                bool hitTerrain = terrainMgr.IsTerrainArea(hit.transform.gameObject);

                if (rightButtonDown)
                {// Если произошло активное ожидание задачи, затем отключить его, поскольку он может быть дополнен только левой кнопкой мыши
                    // If there was an active awaiting task then disable it since it can only be completed with a left mouse button click
                    if (taskMgr.AwaitingTask.IsEnabled)
                    { 
                        taskMgr.AwaitingTask.Disable();
                        return;
                    }

                    if (hitEntity.IsValid())
                        hitEntity.Selection.OnDirectAction();
                    else if (hitTerrain)
                        selectionMgr.GetEntitiesList(EntityType.all, false, true).SetTargetFirstMany(hit.point, playerCommand: true, includeMovement: true);
                }
                else if (leftButtonDown)
                {
                    if(reverseMause == false) {
                    if (!hitEntity.IsValid())
                    {// Полное ожидание задачи на Terreain Нажмите.
                        // Complete awaiting task on terrain click.
                        if (hitTerrain && taskMgr.AwaitingTask.IsEnabled)
                            foreach (IEntityTargetComponent sourceComp in taskMgr.AwaitingTask.Current.sourceTracker.EntityTargetComponents)
                                sourceComp.SetTarget(hit.point, playerCommand: true);
                        // No awaiting task and terrain click = deselecting currently selected entities
                        else// Нет ожидания задачи и Terrain Click = отмените выбор в настоящее время выбранные объекты
                            selectionMgr.RemoveAll();

                        taskMgr.AwaitingTask.Disable();
                        return;
                    }
                    // Ожидание задачи активна с действительной сущностью
                    // Awaiting task is active with a valid hit entity
                    if (taskMgr.AwaitingTask.IsEnabled)
                    {
                        hitEntity.Selection.OnAwaitingTaskAction(taskMgr.AwaitingTask.Current);
                        taskMgr.AwaitingTask.Disable();
                    }// Если нет ожидания задачи, не будет активным, продолжить с регулярным выбором
                    // If no awaiting task is active then proceed with regular selection
                    else  if (hitEntity.GetComponent<GroupArmy>() == true) { hitEntity.GetComponent<GroupArmy>().SelectArmy(); reverseMause = true; }
                    else
                    {
                        selectionMgr.Add(hitEntity, SelectionType.single);
                        reverseMause = true; // тут если ды то когда меняем правую кнопку на левую
                        }
                    }
                    else  //reverseMause == true
                    {
                        if (taskMgr.AwaitingTask.IsEnabled)
                        {
                            taskMgr.AwaitingTask.Disable();
                            reverseMause = false;
                            selectionMgr.RemoveAll(); 
                            taskMgr.AwaitingTask.Disable();
                            return;
                        }

                        if (hitEntity.IsValid())
                        { hitEntity.Selection.OnDirectAction(); reverseMause = false; }
                        else if (hitTerrain) {
                            selectionMgr.GetEntitiesList(EntityType.all, false, true).SetTargetFirstMany(hit.point, playerCommand: true, includeMovement: true);
                        reverseMause = false;
                        }

                        selectionMgr.RemoveAll();

                        taskMgr.AwaitingTask.Disable();
                    }
                }
            }
        }
        #endregion

        #region Handling Double Click Selection
        public void SelectEntitisInRange(IEntity source)
        {
            if (!enableDoubleClickSelect || source.IsFree)
                return;

            foreach (IFactionEntity entity in source.Slot.FactionMgr.FactionEntities)
            {
                if (entity.Code == source.Code
                    && Vector3.Distance(entity.transform.position, source.transform.position) <= doubleClickSelectRange)
                {
                    selectionMgr.Add(entity, SelectionType.multiple);
                }
            }
        }
        #endregion

        #region Handling Selection Flash
        public void FlashSelection(IEntity entity, bool isFriendly)
        {
            if (!entity.IsValid()
                || !entity.SelectionMarker.IsValid())
                return;

            entity.SelectionMarker.StartFlash(
                flashTime,
                flashRepeatTime,
                (isFriendly == true) ? friendlyFlashColor : enemyFlashColor);
        }
        #endregion
    }
}