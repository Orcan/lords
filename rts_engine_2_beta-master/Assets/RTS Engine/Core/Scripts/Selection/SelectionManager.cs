﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using RTSEngine.Entities;
using RTSEngine.Game;
using RTSEngine.Event;

namespace RTSEngine.Selection
{
    public partial class SelectionManager : MonoBehaviour, ISelectionManager
    {
        #region Attributes
        // Key: entity code.  Ключ: Код объекта.
        // Value: list of currently selected entities that share the same code (key).
        // Значение: список выбранных в данный момент объектов, которые разделяют один и тот же код (ключ). 
        private Dictionary<string, List<IEntity>> selectionDic;
        // Кэширует экземпляр объекта, который был впервые выбран среди всех выбранных в настоящее время объектов.
        // Caches the entity instance that was first selected among all of the currently selected entities.
        private IEntity firstSelected;

        /// <summary>
        /// Общее количество выбранных в настоящее время объектов. 
        /// </summary>
        public int Count { private set; get; }
        // кэш последний выбранный тип объекта 
        // Cache the last selected entity type
        private EntityType lastSelectedEntityType = EntityType.all;
        // - это текущий тип выбора эксклюзивна(означает, что выбираются только один и тот же тип). 
        // Is the current selection type exclusive (meaning that only entities of the same type are selected).
        private bool isCurrSelectionExclusive = false;

        [SerializeField, Tooltip("Определите ограничения выбора для типов объектов. ")]
        private EntitySelectionOptions[] selectionOptions = new EntitySelectionOptions[0];

        // Game services // игровые услуги 
        protected ISelectionManager selectionMgr { private set; get; }
        protected IGlobalEventPublisher globalEvent { private set; get; } //что за гломабл Эвенты? 
        protected IMouseSelector mouseSelector { private set; get; } 
        #endregion

        #region Initializing/Terminating
        public void Init(IGameManager gameMgr)
        {
            this.selectionMgr = gameMgr.GetService<ISelectionManager>();
            this.globalEvent = gameMgr.GetService<IGlobalEventPublisher>();
            this.mouseSelector = gameMgr.GetService<IMouseSelector>(); 

            // Initial state
            selectionDic = new Dictionary<string, List<IEntity>>();
            Count = 0;
        }
        #endregion

        #region Testing Entity Selection State
        public bool IsSelected(IEntity entity, bool localPlayerFaction = false)
            => selectionDic.TryGetValue(entity.Code, out List<IEntity> selectedList)
            && selectedList.Contains(entity)
            && (!localPlayerFaction || entity.IsLocalPlayerFaction());

        public bool IsSelectedOnly(IEntity entity, bool localPlayerFaction = false)
            => Count == 1
            && firstSelected == entity
            && (!localPlayerFaction || entity.IsLocalPlayerFaction());
        #endregion

        #region Getting Selected Entities
        public IEntity GetSingleSelectedEntity(EntityType requiredType, bool localPlayerFaction = false)
            => IsSelectedOnly(firstSelected, localPlayerFaction) && firstSelected.IsEntityTypeMatch(requiredType)
                ? firstSelected
                : null;

        public IEnumerable<IEntity> GetEntitiesList(EntityType requiredType, bool exclusiveType, bool localPlayerFaction)
        {
            List<IEntity> entities = new List<IEntity>();

            foreach (string entityCode in selectionDic.Keys)
            {// Обратный объект для тестирования.
                // Reference entity to test.
                IEntity entity = selectionDic[entityCode][0];

                if (localPlayerFaction && !entity.IsLocalPlayerFaction())
                    return Enumerable.Empty<IEntity>();
                // Если это соответствует типу, мы ищем.
                // If this matches the type we're looking for.
                if (entity.IsEntityTypeMatch(requiredType))
                {// используем метод linq.select при добавлении диапазона объектов, чтобы создать новый экземпляр iEnumerable и не завязывать этот с одним в словаре выбора 
                    // Use the Linq.Select method when adding entities range to create a new IEnumerable instance and not tie this one with the one in the selection dictionary
                    entities.AddRange(selectionDic[entityCode].Select(nextEntity => nextEntity));
                    continue;
                }
                // Несоответствие типа сущности, и нам нужны все выбранные объекты, чтобы быть одним и тем же типом. 
                // Entity type mismatch and we need all selected entities to be of the same type.
                if (exclusiveType == true)
                    return Enumerable.Empty<IEntity>();
            }

            return entities;
        }

        public IDictionary<string, IEnumerable<IEntity>> GetEntitiesDictionary(EntityType requiredType, bool localPlayerFaction)
        {
            Dictionary<string, List<IEntity>> entities = new Dictionary<string, List<IEntity>>();

            return selectionDic
                .Where(pair =>
                {
                    return (!localPlayerFaction || pair.Value[0].IsLocalPlayerFaction())
                        && pair.Value[0].IsEntityTypeMatch(requiredType);
                })// используем метод linq.select при добавлении диапазона объектов, чтобы создать новый экземпляр iEnumerable и не завязывать этот с одним в словаре выбора
                  // Use the Linq.Select method when adding entities range to create a new IEnumerable instance and not tie this one with the one in the selection dictionary
                .ToDictionary(pair => pair.Key, pair => pair.Value.Select(nextEntity => nextEntity));
        }
        #endregion

        #region Selecting Entities
        public bool Add(IEnumerable<IEntity> entities)
        {
            RemoveAll();
   // ToList () Для создания отдельной коллекции, чтобы избежать проблемы, в которой входная коллекция ссылается на коллекцию в этом CLAS  // Выберите каждый блок индивидуально 
            // ToList() to generate a separate collection to avoid the issue where the input collection is referencing a collection in this clas
            // Select each unit individually
            return entities
                .ToList()
                .All(entity => Add(entity, SelectionType.multiple));
        }

        public bool Add(IEntity entity, SelectionType type)
        {
            if (!entity.IsValid())
                return false;
            // перезаписать тип выбора, если проигрыватель принуждает множественный выбор 
            // Overwrite the selection type if the player is forcing multiple selection
            if (mouseSelector.MultipleSelectionKeyDown == true)
                type = SelectionType.multiple;
            // Если последний тип выбора был эксклюзивным, и это не соответствует последним выбранным типе объекта
            // принудительно выбирать один выбор (так что все выбранные в настоящее время объекты будут изменены) 
            // If the last selection type was exclusive and this doesn't match the last selected entity type
            // Force single selection now (so that all currently selected entities will be deselected)
            if (isCurrSelectionExclusive && entity.Type != lastSelectedEntityType)
                type = SelectionType.single;
            // Выбор будет помечен как эксклюзив, если объект успешно выбран? по умолчанию нет.
            // Will the selection be marked as exclusive in case the entity is successfully selected? by default no.
            bool exclusiveOnSuccess = false;
            // Найдите текущие параметры выбора типа сущности и обновить соответственно
            // Find the current entity type selection options and update accordingly
            foreach (EntitySelectionOptions options in selectionOptions)
                if (entity.Type == options.entityType)
                {
                    if (options.exclusive == true)
                    {
                        exclusiveOnSuccess = true;
                        // Если последнее выбранное сущность не совпадает с текущим типом
                        // Тип выбора силы для одиноких (все предыдущие выбранные объекты будут отмены) 
                        if (entity.Type != lastSelectedEntityType)
                            type = SelectionType.single;
                    }
                    // Если тип выбора кратный, но это не допускается для этого типа объекта
                    // Тип выбора силы для одиноких (все предыдущие выбранные объекты будут отмены)
                    // If the selection type is multiple but that's not allowed for this entity type
                    // Force selection type to single (All previous selected entities will be deselected)
                    if (type == SelectionType.multiple && options.allowMultiple == false)
                        type = SelectionType.single;

                    break;
                }

            switch (type)
            {
                case SelectionType.single:
                    // удалить все выбранные в настоящее время объекты
                    // Remove all currently selected entities
                    RemoveAll();

                    break;

                case SelectionType.multiple: //multiple selection:// Многократный выбор:
                    // Если множественная клавиша выбора down & Entity уже выбрана -> Удалите его из уже выбранной группы и остановитесь здесь. 
                    // If the multiple selection key is down & entity is already selected -> remove it from the already selected group and stop here.
                    if (mouseSelector.MultipleSelectionKeyDown && IsSelected(entity))
                    {
                        Remove(entity);
                        return false;
                    }

                    break;
            }

            if (entity.Selection.CanSelect && !IsSelected(entity))
            {// Если есть хотя бы другое сущность того же типа, который уже выбран, а затем добавьте эту сущность в тот же список 
                // If there's at least another entity of the same type that is already selected then add this entity to the same list
                if (selectionDic.TryGetValue(entity.Code, out List<IEntity> targetList))
                    targetList.Add(entity);
                else
                    selectionDic.Add(entity.Code, new List<IEntity> { entity });

                Count++;
                // Установить FirstSelected Entity 
                // Set firstSelected entity
                if (Count == 1)
                    firstSelected = entity;

                entity.Selection.OnSelected();

                globalEvent.RaiseEntitySelectedGlobal(entity);

                lastSelectedEntityType = entity.Type;
                isCurrSelectionExclusive = exclusiveOnSuccess;

                return true;
            }

            return false;
        }
        #endregion

        #region Deselecting Entities
        public void Remove(IEnumerable<IEntity> entities)
        {// ToList () Для создания отдельного сбора, чтобы избежать проблемы, в которой входная коллекция ссылается на коллекцию в этом классе.
            // ToList() to generate a separate collection to avoid the issue where the input collection is referencing a collection in this class.
            foreach (IEntity entity in entities.ToList())
                Remove(entity);
        }

        public bool Remove(IEntity entity)
        {
            if (selectionDic.TryGetValue(entity.Code, out List<IEntity> selectedList))
            {
                if (!selectedList.Remove(entity))
                    return false;

                Count--;
                // Если список выбора теперь пуст, удалите всю запись из словаря
                // If the selection list is now empty, remove the whole entry from the dictionary
                if (selectedList.Count == 0)
                    selectionDic.Remove(entity.Code);
                // Если только один объект вышел, назначьте его как единое выделенное сущность
                if (Count == 1) //if only one entity is left selected, assign it as the single selected entity
                    firstSelected = GetEntitiesList(EntityType.all, false, false).FirstOrDefault();

                entity.Selection.OnDeselected();

                globalEvent.RaiseEntityDeselectedGlobal(entity);

                return true;
            }

            return false;
        }

        public void RemoveAll()
        {// Копировать ключи в новый массив, потому что DIC будет изменен во время контура Foreach.
            // Copy keys into new array because the dic will be modified during the foreach loop.
            IEnumerable<string> keys = selectionDic.Keys.ToList();

            Count = 0; //reset selection count// Сброс подсчета выбора

            foreach (string code in keys)
            {
                List<IEntity> selectedList = selectionDic[code];
                selectionDic.Remove(code);

                while (selectedList.Count > 0)
                {
                    IEntity nextEntity = selectedList[0];
                    selectedList.RemoveAt(0);

                    nextEntity.Selection.OnDeselected();

                    globalEvent.RaiseEntityDeselectedGlobal(nextEntity);
                }
            }

            // Reset state // Сброс состояния
            selectionDic.Clear();
            firstSelected = null;
        }
        #endregion
    }
}
