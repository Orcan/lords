﻿using System.Collections.Generic;

using UnityEngine;

using RTSEngine.Effect;
using RTSEngine.Entities;
using RTSEngine.Game;

namespace RTSEngine.Upgrades
{
    public abstract class Upgrade : MonoBehaviour, IMonoBehaviour
    {
        [SerializeField, EnforceType(typeof(IEntity)), Tooltip("Объединение источника, обновление которого обрабатывается этим компонентом (требуется обновления компонентов объекта, но необязательно для обновлений объекта  Source entity whose upgrade is handled by this component (required for entity component upgrades but optional for entity upgrades).")]
        private GameObject sourceEntity = null;
        public IFactionEntity SourceEntity => sourceInstanceOnly 
            ? gameObject.GetComponent<IFactionEntity>() 
            : (sourceEntity.IsValid() ? sourceEntity.GetComponent<IFactionEntity>() : null);
        public abstract string SourceCode { get; }

        [Space(), SerializeField, Tooltip("Обновите только экземпляр источника, который имеет этот компонент к нему?  Upgrade only the source instance that has this component attached to it?")]
        private bool sourceInstanceOnly = false;
        public bool SourceInstanceOnly => sourceInstanceOnly;

        [SerializeField, Tooltip("Обновление уже порожденные экземпляры? Upgrade already spawned instances?")]
        private bool updateSpawnedInstances = true;
        public bool UpdateSpawnedInstances => updateSpawnedInstances;

        [Space(), SerializeField, EnforceType(typeof(IEffectObject), prefabOnly: true), Tooltip("Эффект, показанный при улучшении порожденных экземпляров. Effect shown when spawned instances are upgraded.")]
        private GameObject upgradeEffect = null;
        public IEffectObject UpgradeEffect => upgradeEffect.IsValid() ? upgradeEffect.GetComponent<IEffectObject>() : null;

        [Space(), SerializeField, Tooltip("Обновления для запуска / запуска, когда это обновление завершено Upgrades to trigger/launch when this upgrade is completed.")]
        private Upgrade[] triggerUpgrades = new Upgrade[0];
        public IEnumerable<Upgrade> TriggerUpgrades => triggerUpgrades;
        // Зачем предоставить экземпляр igameanager в качестве ввода? Потому что компоненты обновления не всегда прикреплены к порожденным экземплярам, но иногда для префицев
        // Why provide the IGameManager instance as an input? Because Upgrade components are not always attached to spawned instances but sometimes to prefabs
        public abstract void LaunchLocal(IGameManager gameMgr, int factionID);
    }
}
