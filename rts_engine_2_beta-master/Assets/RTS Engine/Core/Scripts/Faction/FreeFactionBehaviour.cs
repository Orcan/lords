﻿using RTSEngine.Entities;
using System;
using UnityEngine;

namespace RTSEngine.Faction
{
    [System.Serializable]
    public struct FreeFactionBehaviour
    {
        [Tooltip("Разрешить взаимодействие с другими субъектами свободных фракций? Allow interaction with other free faction entities?")]
        public bool allowFreeFaction;
        [Tooltip("Разрешить взаимодействие с локальной фракцией игрока? Allow interaction with the local player faction?")]
        public bool allowLocalPlayer;
        [Tooltip("Разрешить взаимодействие с объектами фракций, которые не являются ни местными игроками, ни субъектами свободных фракций? Allow interaction with faction entities that are neither the local player nor free faction entities?")]
        public bool allowRest;

        public bool IsEntityAllowed(IUnit unit)
        {
            if (unit.IsFree)
                return allowFreeFaction;
            else if (unit.IsLocalPlayerFaction())
                return allowLocalPlayer;

            return allowRest;
        }
    }
}
