﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using RTSEngine.ResourceExtension;
using RTSEngine.Entities;
using RTSEngine.Event;
using RTSEngine.Determinism;
using RTSEngine.NPC.Event;
using RTSEngine.Logging.NPC;

namespace RTSEngine.NPC.UnitExtension
{
    public partial class NPCUnitCreator : NPCComponentBase, INPCUnitCreator
    {
        #region Attributes
        [SerializeField, EnforceType(typeof(IUnit), prefabOnly: true)
            , Tooltip("Установка префидентов, что этот компонент способен создавать независимо от других компонентов NPC. Универсальные префикции, которые назначены другим компонентам NPC (такие как коллекционеры или строители ресурсов), не обязательны для его добавления в этот список" +
            "Unit prefabs that this component is able to create independently from other NPC components. Unit prefabs that are assigned to other NPC components (such as resource collectors or builders) are not required to be added to this list.")]
        private List<GameObject> independentUnits = new List<GameObject>();

        [SerializeField, Tooltip("Тип ресурса емкости, который используется в качестве ресурса населения. Если это не назначено, фракция NPC будет стремиться создать только минимальное количество каждого типа блока" +
            "Capacity resource type that is used as the population resource. If this is not assigned, the NPC faction will aim to create just the minimum amount of each unit type.")]
        private FactionTypeFilteredResourceType populationResource = new FactionTypeFilteredResourceType();
        public ResourceTypeInfo PopulationResource { private set; get; } = null;
        // Клавиша: тип блока / код // Значение: ActiveUnitregulator, который управляет типом устройства.
        // Key: unit type/code  // Value: ActiveUnitRegulator that manages the unit type.
        private Dictionary<string, NPCActiveUnitRegulatorData> activeUnitRegulators;
        #endregion

        #region Initiliazing/Terminating
        protected override void OnPreInit()
        {
            PopulationResource = populationResource.GetFiltered(gameMgr.GetFactionSlot(factionMgr.FactionID).Data.type);
            // Ни один ресурс населения не был установлен, только минимальное количество единиц будет создано автоматически!
            if (logger.RequireValid(PopulationResource, $"[{GetType().Name} - Faction ID: {factionMgr.FactionID}] No population resource has been set, only the minimum amount of units will be auto-created!", type: Logging.LoggingType.warning))
             //   Должен быть ресурс емкости, который будет использоваться в качестве основного ресурса населения!
                logger.RequireTrue(PopulationResource.HasCapacity, $"[{GetType().Name} - Faction ID: {factionMgr.FactionID}] Resource '{PopulationResource.Key}' Must be a capacity resource to be used as the main population resource!");

            activeUnitRegulators = new Dictionary<string, NPCActiveUnitRegulatorData>();
        }

        protected override void OnPostInit()
        {// Активируйте независимые типы единиц
            // Activate the independent unit types
            foreach (IUnit unit in independentUnits.Select(obj => obj.IsValid() ? obj.GetComponent<IUnit>() : null))
                ActivateUnitRegulator(unit);

            globalEvent.UnitUpgradedGlobal += HandleUnitUpgradeGlobal;
        }

        protected override void OnDestroyed()
        {
            globalEvent.UnitUpgradedGlobal -= HandleUnitUpgradeGlobal;

            DestroyAllActiveRegulators();
        }
        #endregion

        #region Handling Events: Unit Upgrade
        private void HandleUnitUpgradeGlobal(IUnit unit, UpgradeEventArgs<IEntity> args)
        {
            if (!factionMgr.FactionID.IsSameFaction(args.FactionID))
                return;

            DestroyActiveRegulator(unit.Code);
            ActivateUnitRegulator(args.UpgradeElement.target as IUnit);
        }
        #endregion

        #region Handling Unit Regulators 
        public NPCUnitRegulator ActivateUnitRegulator(IUnit unitPrefab)
        {
            if (!logger.RequireValid(unitPrefab,
            //   Не удается активировать регулятор для префаба для недействительного блока, проверьте список «независимых единиц» для неназначенных элементов или любого другого ввода устройства в других компонентах NPC
                $"[{GetType().Name} - Faction ID: {factionMgr.FactionID}] Can not activate a regulator for an invalid unit prefab, check the 'Independent Units' list for unassigned elements or any other unit input field in other NPC components."))
                return null;
            // Если нет действительных данных регулятора для устройства не возвращается, не продолжайте
            // If no valid regulator data for the unit is returned then do not continue
            NPCUnitRegulatorData regulatorData = unitPrefab.GetComponent<NPCUnitRegulatorDataInput>()?.GetFiltered(factionType: factionSlot.Data.type, npcType: npcMgr.Type);
            if (!regulatorData.IsValid())
                return null;
            // Если есть активный регулятор для текущего типа блока
            // If there is an active regulator for the current unit type
            NPCUnitRegulator activeInstance = GetActiveUnitRegulator(unitPrefab.Code);
            if (activeInstance.IsValid())
                return activeInstance;
            // На данном этапе есть действительные данные для регулятора, и он еще не создан
            // At this stage, there is valid data for the regulator and it has not been created yet
            NPCActiveUnitRegulatorData newUnitRegulator = new NPCActiveUnitRegulatorData()
            {
                instance = new NPCUnitRegulator(regulatorData, unitPrefab, gameMgr, npcMgr),
                // Начальный таймер нереста: регулярное поле для перезагрузки + начать создавать после задержки
                // Initial spawning timer: regular spawn reload + start creating after delay
                spawnTimer = new TimeModifiedTimer(regulatorData.CreationDelayTime + regulatorData.SpawnReload)
            };

            newUnitRegulator.instance.AmountUpdated += HandleUnitRegulatorAmountUpdated;

            activeUnitRegulators.Add(unitPrefab.Code, newUnitRegulator);
            // всякий раз, когда новый регулятор добавляется в список активных регуляторов, затем переместите создатель устройства в активное состояние
            // Whenever a new regulator is added to the active regulators list, then move the unit creator into the active state
            IsActive = true;

            return newUnitRegulator.instance;
        }
        // Получить активный блок регулятора
        public NPCUnitRegulator GetActiveUnitRegulator(string unitCode)
        {
            if (activeUnitRegulators.TryGetValue(unitCode, out NPCActiveUnitRegulatorData nextUnitRegulator))
                return nextUnitRegulator.instance;

            return null;
        }

        private void DestroyAllActiveRegulators()
        {
            foreach (NPCActiveUnitRegulatorData nextUnitRegulator in activeUnitRegulators.Values)
            {
                nextUnitRegulator.instance.AmountUpdated -= HandleUnitRegulatorAmountUpdated;

                nextUnitRegulator.instance.Disable();
            }

            activeUnitRegulators.Clear();
        }

        private void DestroyActiveRegulator(string unitCode)
        {
            NPCUnitRegulator nextUnitRegulator = GetActiveUnitRegulator(unitCode);
            if (nextUnitRegulator.IsValid())
            {
                nextUnitRegulator.AmountUpdated += HandleUnitRegulatorAmountUpdated;

                nextUnitRegulator.Disable();
            }

            activeUnitRegulators.Remove(unitCode);
        }
        #endregion

        #region Handling Events: Unit Regulator
        private void HandleUnitRegulatorAmountUpdated(NPCRegulator<IUnit> unitRegulator, NPCRegulatorUpdateEventArgs args)
        {
            // В случае, если этот компонент неактивен, а один из существующих (не ожидающих) единиц удален ...
            // или когда целевой подсчет типа блока выше текущего подсчета
            // In case this component is inactive and one of the existing (not pending) units is removed ...
            // Or when the target count of the unit type is higher than the current count
            if (!IsActive
                && !unitRegulator.HasTargetCount)
                IsActive = true;
        }
        #endregion

        #region Handling Unit Creation
        protected override void OnActiveUpdate()
        {// Предположим, что создатель подразделения закончил свою работу с текущими регуляторами активных блоков.
            // Assume that the unit creator has finished its job with the current active unit regulators.
            IsActive = false;

            foreach (NPCActiveUnitRegulatorData nextUnitRegulator in activeUnitRegulators.Values)
            {// Если мы можем автоматически создавать это:
                //if we can auto create this:
                if (nextUnitRegulator.instance.Data.CanAutoCreate
                    && !nextUnitRegulator.instance.HasTargetCount)
                {// Чтобы сохранить этот компонент мониторинг создания следующих единиц
                    // To keep this component monitoring the creation of the next units 
                    IsActive = true;

                    if (nextUnitRegulator.spawnTimer.ModifiedDecrease())
                    {
                        nextUnitRegulator.spawnTimer.Reload(nextUnitRegulator.instance.Data.SpawnReload);

                        OnCreateUnitRequestInternal(
                            nextUnitRegulator.instance,
                            nextUnitRegulator.instance.TargetCount - nextUnitRegulator.instance.Count,
                            out _);
                    }
                }
            }
        }

        public bool OnCreateUnitRequest(string unitCode, int requestedAmount, out int createdAmount)
        {
            createdAmount = 0;

            NPCUnitRegulator nextUnitRegulator = GetActiveUnitRegulator(unitCode);
            if (nextUnitRegulator?.Data.CanCreateOnDemand == false)
                return false;

            return OnCreateUnitRequestInternal(nextUnitRegulator, requestedAmount, out createdAmount);
        }

        private bool OnCreateUnitRequestInternal(NPCUnitRegulator instance, int requestedAmount, out int createdAmount)
        {
            createdAmount = 0;

            if (!instance.IsValid()
                || instance.HasReachedMaxAmount
                || requestedAmount <= 0)
                return false;
            // Если для этого блока нет задач, назначенных задач, которые могут создавать экземпляры устройств ...
            // If there are no task launchers assigned to this unit regulator that can create instances of the units...
            if (instance.CreatorsCount == 0)
                // Функция будущей: Разрешить фракцию NPC сканировать свои доступные единицы / здания для создания того, что может производить этот тип блока
                // FUTURE FEATURE: Allow NPC faction to scan its available units/buildings to create one that can produce this unit type
                return false;

            createdAmount = requestedAmount;
            instance.Create(ref requestedAmount);

            createdAmount -= requestedAmount;

            return true;
        }
        #endregion

#if UNITY_EDITOR

        [Header("Logs")]
        [SerializeField, ReadOnly, Space()]
        //NPC Active Fation Contity Regulator Журнал данных журнала
        private NPCActiveFactionEntityRegulatorLogData[] activeUnitRegulatorLogs = new NPCActiveFactionEntityRegulatorLogData[0];

        protected override void UpdateLogStats()
        {
            activeUnitRegulatorLogs = activeUnitRegulators.Values
                .Select(regulator => new NPCActiveFactionEntityRegulatorLogData(
                    regulator.instance,
                    spawnTimer: regulator.spawnTimer.CurrValue,
                    creators: regulator.instance.Creators.Select(creator => $"{creator.Entity.Key}: {creator.Entity.Code}").ToArray()
                    ))
                .ToArray();
        }
#endif
    }
}
