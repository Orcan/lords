﻿using RTSEngine.Determinism;
using UnityEngine;

namespace RTSEngine.NPC.UnitExtension
{
    [System.Serializable]
    public struct NPCFactionEntityForceCreationData // Данные создания силы Фракции NPC
    {
        [Tooltip("Разрешить периодически обновлять целевую сумму типа (ы) объекта фракции, чтобы заставить фракцию NPC создать больше этого?" +
            "Allow to periodically update the target amount of the faction entity type(s) to force the NPC faction to create more of it?")]
        public bool enabled;

        [Tooltip("Задержки обновления целевого количества типа (ы) сущности фракции после начала игры" +
            "Delays updating the target count of the faction entity type(s) after the game starts.")]
        public FloatRange targetCountUpdateDelay;

        [Tooltip("Как часто для увеличения целевого количества типа сущностей фракции" +
            "How often to increase the target count of the faction entity type(s)?")]
        public FloatRange targetCountUpdatePeriod;
        [HideInInspector]
        public TimeModifiedTimer timer;

        [Tooltip("Сумма для обновления целевого количества типа (ы) сущностей фракций каждый период." +
            "Amount to update the target count of the faction entity type(s) each period.")]
        public int targetCountUpdateAmount;
    }
}
