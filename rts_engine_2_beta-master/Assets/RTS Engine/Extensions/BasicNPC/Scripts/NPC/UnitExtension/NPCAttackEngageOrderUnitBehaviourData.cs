﻿using UnityEngine;

namespace RTSEngine.NPC.UnitExtension
{
    [System.Serializable]
    public struct NPCAttackEngageOrderUnitBehaviourData // Атака NPC включает в себя данные заказа единицы поведения
    {
        [Tooltip("Включить отправку экземпляров устройств в цель атаки / назначения, когда он предоставляется. Enable to send the unit instances to the attack target/destination when one is provided.")]
        public bool send;
        [Tooltip("Только отправка единичных экземпляров, которые находятся в состоянии холостого состояния? Only send unit instances that are in an idle state?")]
        public bool sendIdleOnly;
        [Tooltip("Только отправлять единицы экземпляров, которые имеют активную цель, которая не может атаковать обратно? Only send unit instances who have an active target that can not attack back?")]
        public bool sendNoTargetThreatOnly;

        [Tooltip("Соотношение экземпляров единиц для отправки всех доступных для отправки экземпляров, которые будут поручены с переходом в цель атаки / назначения. <= 0,0F означает, что никакие экземпляры не будут отправлены и> = 1.0f означает, что все доступные экземпляры будут отправлены " +
            "Ratio of the unit instances to send over all available to send instances that will be tasked with moving to the attack target/destination. <=0.0f means that no instances will be sent and >=1.0f means all available instances will be sent")]
        public FloatRange sendRatioRange;

        [Tooltip("Время задержки до того, как доступные и подходящие экземпляры единиц отправляются на цель атаки / назначения? Delay time before the available and eligible unit instances are sent to the attack target/destination?")]
        public FloatRange sendDelay;

        [Tooltip("Включить начать фактическую атаку с экземплярами устройства для отправки. Или отключить только для перемещения подразделений только к цели атаки / назначения." +
            "Enable to launch an actual attack with the unit instances to send. Or disable to only move the units towards the attack target/destination.")]
        public bool attack;

        [Tooltip("Отправьте задние подразделения к их появлению позиций, когда атака отменена? Send back units to their spawn positions when the attack is cancelled?")]
        public bool sendBackOnAttackCancel;
    }
}
