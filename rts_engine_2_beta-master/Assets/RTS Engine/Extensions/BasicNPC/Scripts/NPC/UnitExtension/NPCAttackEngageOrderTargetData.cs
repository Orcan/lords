﻿using RTSEngine.Determinism;
using RTSEngine.Entities;
using UnityEngine;

namespace RTSEngine.NPC.UnitExtension
{//Атака NPC участвует в целевых данных заказа
    public struct NPCAttackEngageOrderTargetData 
    {
        public IFactionEntity target;
        public Vector3 targetPosition;

        public TimeModifiedTimer delayTimer;
    }
}
