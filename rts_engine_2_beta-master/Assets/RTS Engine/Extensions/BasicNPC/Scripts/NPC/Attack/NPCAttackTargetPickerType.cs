﻿namespace RTSEngine.NPC.Attack
{// Как фракция NPC выбирает целевую фракцию для атаки?
    // How does a NPC faction pick a target faction to attack?
    public enum NPCAttackTargetPickerType { random, mostAttackResources, leastAttackResources }
}
