﻿using RTSEngine.Task;
using UnityEngine;

/* EntityComponentTaskUI script created by Oussama Bouanani, SoumiDelRio.
 * This script is part of the Unity RTS Engine */

namespace RTSEngine.UI
{
    /// <summary>
    /// /// содержит информацию о элементах пользовательских интерфейсов задачи, которая принадлежит компоненту, которая реализует интерфейс iEntityComponent и прикреплен к экземпляру объекта. 
    /// Holds information regarding the UI elements of a task that belongs to a component that implements IEntityComponent interface and attached to an Entity instance.
    /// </summary>
    [System.Serializable]
    public struct EntityComponentTaskUIData
    {
        /// <summary>
        ////// Определяет различные типы дисплея для задачи, связанной с структурой EntityComponentTaskui.
        /// СИГНЕСЕССЕЛЬФИКТ: Только отображать задачу, если объект выбран единственный.
        /// HomumumultiPleseLection: Только отображается задача, если выбраны только объекты одного и того же типа.
        /// Гетеромультиплаилекция: отображение задачи до тех пор, пока выбирается объект источника.
        ///
        /// 
        /// Defines the different display types for the task associated with the EntityComponentTaskUI struct.
        /// singleSelection: Only display task if the entity is the only one selected.
        /// homoMultipleSelection: Only display task if only entities of the same type are selected.
        /// heteroMultipleSelection: Display task as long as the source entity is selected.
        /// </summary>
        public enum DisplayType {singleSelection, homoMultipleSelection, heteroMultipleSelection }

        [Tooltip("Уникальный код для каждой задачи. Unique code for each task.")]
        public string code;

        [Tooltip("При отключении соответствующая задача не может отображаться на панели задач. When disabled, the associated task can not be displayed in the task panel.")]
        public bool enabled;

        [Tooltip("Условия выбора для отображения соответствующей задачи. Selection conditions to display the associated task.")]
        public DisplayType displayType;

        [Tooltip("Спрайт будет использоваться для значка задач. The sprite to be used for the task's icon.")]
        public Sprite icon;
        [Tooltip("Категория панели задач пользовательской интерфейсы, где задача будет размещена. The category of the UI task panel where the task will be placed at.")]
        public int panelCategory;
        [Tooltip("Включите натянутую задачу на определенном слоте категории панели. Enable to force the task to be drawn on a specific slot of the panel category.")]
        public bool forceSlot;
        [Tooltip("Индекс слота, чтобы нарисовать задачу. Index of the slot to draw the task in."), Min(0)]
        public int slotIndex;

        [Tooltip("Покажите описание задачи в подсказке, когда мышь охватывает задачу? Show a description of the task in the tooltip when the mouse hovers over the task?")]
        public bool tooltipEnabled;
        [Tooltip("Описание задачи, которая появится на подсказке панели задач. Description of the task that will appear in the task panel's tooltip.")]
        public string description;
        [Tooltip("Скрыть всплывающую подсказку (если она была включена), когда задача нажала? Hide tooltip (if it was enabled) when the task is clicked?")]
        public bool hideTooltipOnClick;

        [Tooltip("В случае включения времени перезагрузки (ожидающая задача), это его продолжительность. Действителен только тогда, когда задача запускается для ожидающих задач.In case reload time is enabled (pending task), this is its duration. Only valid when the task launcher allows for pending tasks."), Min(0)]
        public float reloadTime;

        [Tooltip("Когда задача назначается как ожидание задачи, это представляет значок, который будет использоваться в качестве текстуры курсора мыши. Если не назначен, основной значок используется в качестве текстуры курсора. When the task is assigned as the awaiting task, this represents the icon that will be used as the mouse cursor's texture. If not assigned, the main icon is used as the cursor's texture.")]
        public TaskCursorData cursor;
    }
}
