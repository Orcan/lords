﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using RTSEngine.Entities;
using RTSEngine.Game;
using RTSEngine.Cameras;
using RTSEngine.Logging;
using RTSEngine.BuildingExtension;
using RTSEngine.Event;
using RTSEngine.UI;
using RTSEngine.Selection;
using UnityEngine.UI;
using RTSEngine.Health;

public class GroupArmyManager : MonoBehaviour
{
    // ArmyLevelList  сюда нужно перекидывать Полководцев которые можно разместить в игре 
    // DicneryArmySpawnList  сюда перекидываются войскакогторый создал герой чтоб потом на кнопку их вызывать 
    protected IGameManager gameMgr { private set; get; }
    protected ISelectionManager selectionMgr { private set; get; } 
    public GroupArmyList[] groupArmyList; 
    // 
    void Start()
    {
        Invoke(nameof(Init), 1);
        InvokeRepeating(nameof(ManyLiveInArmy), 1, 1);
    }
    public void Init()    //(IGameManager gameMgr)
    {
        Debug.Log("ПРОШЛО");
        this.gameMgr = MM.gMng;
        this.selectionMgr = gameMgr.GetService<ISelectionManager>();


        for (int i = 0; i < groupArmyList.Length; i++)
        {
            foreach (Entity unit in groupArmyList[i].unitEntityInArmy) // вот самое главное 
            {
                if (unit.gameObject.GetComponentInChildren<GroupArmy>() == null)
                {
                    unit.gameObject.AddComponent<GroupArmy>().IdArmy = i;
                    unit.gameObject.GetComponent<GroupArmy>().groupArmyManager = this;
                }
                else
                {
                    unit.gameObject.GetComponent<GroupArmy>().IdArmy = i;
                }

            }

        }




    }

    // тут нужно когда когда создавалась новая армия она появлялась 
    //void SpawnArmy()
    //{
    //    selectionMgr.RemoveAll();

    //    foreach (Entity unit in unitEntity) // вот самое главное 
    //    {
    //        selectionMgr.Add(unit, SelectionType.multiple);
    //    }

    //}
    // тут нужно когда нажимаешь на 1 2 3 вызывались дикнери армии 
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SelectionArmyN(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SelectionArmyN(2);
        }
    }

    public void SelectionArmyN(int _namber)
    {
        MM.singlton.mouseSelector.reverseMause = true;
        selectionMgr.RemoveAll();


        foreach (Entity unit in groupArmyList[_namber].unitEntityInArmy) // вот самое главное 
        {
            selectionMgr.Add(unit, SelectionType.multiple);
        }
    }


    void ManyLiveInArmy()
    {
    
        List<int> countLive = new List<int>(); 
        GroupArmy arm = null; 
            for (int i = 0; i < groupArmyList.Length; i++)
        {
            countLive.Add(0);
            for (int g = 0; g < groupArmyList[i].unitEntityInArmy.Count; g++)
            {

                if (groupArmyList[i].unitEntityInArmy[g] != null)
                {
                    countLive[i]  += 1;
               
                }
            }
        }
        for (int i = 0; i < groupArmyList.Length; i++)
        {
            for (int g = 0; g < groupArmyList[i].unitEntityInArmy.Count; g++)
            {
         
                if (groupArmyList[i].unitEntityInArmy[g] != null)
                {
                    arm = groupArmyList[i].unitEntityInArmy[g].gameObject.GetComponent<GroupArmy>();
 
                    if (arm != null && arm.sliderCommaner != null)
                    {
    

                        arm.sliderCommaner.value = (100f / (groupArmyList[i].unitEntityInArmy.Count - 1) * (countLive[i] - 1)) / 100f;
                        if (countLive[i] == 1)
                        {
                            Destroy(groupArmyList[i].unitEntityInArmy[g].gameObject, 1f);
                        };

                    }
                }
            }
        }
    }
}







//countLive = 0;
//for (int i = 0; i < wunitEntity.Count; i++)
//{

//    if (unitEntity[i] != null)
//    {
//        countLive++;

//    }
//}
//foreach (Entity unit in unitEntity) // вот самое главное 
//{
//    if (unit.GetComponent<GroupArmy>().commandir == true)
//    {
//        unit.gameObject.GetComponent<GroupArmy>().sliderCommaner.value = (100 / unitEntity.Count * countLive)/100;    // =(100/C62*C63)/100
//    };
//}

[Serializable]
public struct GroupArmyList
{
    public List<Entity> unitEntityInArmy;
}


