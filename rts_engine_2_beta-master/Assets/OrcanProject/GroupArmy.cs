﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroupArmy : MonoBehaviour
{
    public int IdArmy = 0;
    public GroupArmyManager groupArmyManager;
    public bool commandir = false;
    public Slider sliderCommaner = null;
    public void Init(GroupArmyManager _groupArmyManager, int _IdArmy)  // GroupArmyManager
    {
        this.groupArmyManager = _groupArmyManager;
        IdArmy = _IdArmy;

        if (commandir == true)
        {
            sliderCommaner =  GetComponentInChildren<Slider>();    // =(100/C62*C63)/100
        };
        //   GetComponentInChildren     // нужно найти в нем TaskMovement и он будет рисовать Rander
    } 
        public void SelectArmy() // MouseSelector 
    {
      //  if (commandir) { }
        groupArmyManager.SelectionArmyN(IdArmy);
    }
}