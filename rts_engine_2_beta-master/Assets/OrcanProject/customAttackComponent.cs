using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using RTSEngine;
using RTSEngine.Game;
using RTSEngine.Event;
using RTSEngine.Entities;
using RTSEngine.EntityComponent;
using RTSEngine.Attack;
using RTSEngine.Service;
using RTSEngine.UnitExtension;

public class customAttackComponent : MonoBehaviour, IEntityPostInitializable
{

    public bool IsActive = false;

    IUnit unit;

    IGameManager gameManager;

    IAttackComponent attackComponent;
    AttackLauncher attackLauncher;
    AttackObject attackObject = null;
    bool trackingHarpoon = false;

    //UnitAttack unitAttack;
    IAttackManager attackManager;

    private bool targetLocked = false;
    private bool weaponFired = false;

    private LineRenderer rope = null;
    private bool targetHookedUp = false;
    bool trackingProjectile = false;

    public void OnEntityPostInit(IGameManager gameMgr, IEntity entity)
    {

        gameManager = gameMgr;

        attackManager = gameManager.GetService<IAttackManager>();

        attackComponent = entity.AttackComponent;

        unit = entity as IUnit;

        attackLauncher = unit.AttackComponent.Launcher;

        attackLauncher.AttackLaunched += TrackProjectile;

    }


    public void Disable()
    {

        attackLauncher.AttackLaunched -= TrackProjectile;
    }
 
    public void TrackProjectile(AttackLauncher attackLauncher, AttackLaunchEventArgs attackLaunchEventArgs)
    {
        attackObject = attackLaunchEventArgs.LaunchLog.attackObject as AttackObject;
         trackingProjectile = true;
    }

    public void TargetLocked()
    {
        if(unit != null)
        {
            if (unit.AttackComponent.Target.instance != null)
            {
                targetLocked = true;
                print("Target: " + unit.AttackComponent.Target.instance.Name + " - " + unit.AttackComponent.Target.instance.transform.position);
            }
        }
    }

    public void LaunchedProjectile()
    {
        if(unit != null)
        {
            if (unit.AttackComponent.Target.instance != null)
            {
                if (!unit.AttackComponent.Launcher.Sources[0].launchPosition.gameObject.GetComponent<LineRenderer>())
                {
                    rope = unit.AttackComponent.Launcher.Sources[0].launchPosition.gameObject.AddComponent<LineRenderer>();
                }
                else
                {
                    rope = unit.AttackComponent.Launcher.Sources[0].launchPosition.gameObject.GetComponent<LineRenderer>();
                }
                
                rope.positionCount = 2;
                rope.startWidth = 0.2f;
                rope.endWidth = 0.2f;
                rope.SetPosition(0,unit.AttackComponent.Launcher.Sources[0].launchPosition.position);
                weaponFired = true;
            }
        }
    }

    public void HarpoonImpact()
    {
        
        if(attackObject != null && rope != null)
        {
           rope.SetPosition(1, attackObject.transform.position);
        }
    }

    public void Update()
    {
        if (attackObject != null && trackingProjectile == true && rope != null)
        {
            rope.SetPosition(1, attackObject.transform.position);
        }

    }



}
